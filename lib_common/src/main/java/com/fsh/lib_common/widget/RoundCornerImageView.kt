package com.fsh.lib_common.widget

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import com.fsh.lib_common.R
import com.fsh.lib_common.ext.getMatrixByReflect

class RoundCornerImageView : ImageView {
    private var corner:Int = 0
    private var strokeColor:Int = Color.TRANSPARENT
    private var strokeWidth:Int = 0
    private val mPaint:Paint
    private var mShader:BitmapShader? = null

    constructor(context: Context?) : this(context,null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs,0)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ){
        val osa =
            context?.obtainStyledAttributes(attrs, R.styleable.RoundCornerImageView)
        corner = osa?.getDimensionPixelSize(R.styleable.RoundCornerImageView_corner,corner) ?: corner
        strokeWidth = osa?.getDimensionPixelSize(R.styleable.RoundCornerImageView_strokeWidth,strokeWidth) ?: strokeWidth
        strokeColor = osa?.getColor(R.styleable.RoundCornerImageView_android_strokeColor,strokeColor) ?: strokeColor
        osa?.recycle()
        mPaint = Paint().also {
            it.isAntiAlias = true
            it.strokeWidth = strokeWidth.toFloat()
        }
    }



    override fun onDraw(can: Canvas?) {
        val canvas = can ?: return
        val drawable:Drawable = drawable ?: return
        val shader = mShader ?: return
        val mDrawableWidth = drawable.intrinsicWidth
        val mDrawableHeight = drawable.intrinsicHeight
        if(mDrawableWidth == 0 || mDrawableHeight == 0){
            return
        }
        val matrix = getMatrixByReflect()
        mPaint.shader = shader
        if(matrix == null && paddingLeft == 0 && paddingTop == 0){
            canvas.drawRoundRect(0F,0F,mDrawableWidth.toFloat(),mDrawableHeight.toFloat(),corner.toFloat(),corner.toFloat(),mPaint)
        }else{
            canvas.drawRoundRect(paddingLeft.toFloat(),paddingTop.toFloat(),paddingLeft+mDrawableWidth.toFloat(),paddingTop+mDrawableHeight.toFloat(),corner.toFloat(),corner.toFloat(),mPaint)
        }
        mPaint.shader = null
        if(strokeWidth > 0){
            mPaint.style = Paint.Style.STROKE
            mPaint.color = strokeColor
            if(matrix == null && paddingLeft == 0 && paddingTop == 0){
                canvas.drawRoundRect(0F,0F,mDrawableWidth.toFloat(),mDrawableHeight.toFloat(),corner.toFloat(),corner.toFloat(),mPaint)
            }else{
                canvas.drawRoundRect(paddingLeft.toFloat(),paddingTop.toFloat(),paddingLeft+mDrawableWidth.toFloat(),paddingTop+mDrawableHeight.toFloat(),corner.toFloat(),corner.toFloat(),mPaint)
            }
        }
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        bm?.let {
            mShader = BitmapShader(it,Shader.TileMode.CLAMP,Shader.TileMode.CLAMP)
        } ?: run {
            mShader = null
        }
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        updateDrawable()
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        updateDrawable()
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        updateDrawable()
    }

    private fun updateDrawable(){
        drawable?.let {
            mShader = BitmapShader(it.toBitmap(),Shader.TileMode.CLAMP,Shader.TileMode.CLAMP)
        } ?: run {
            mShader = null
        }
    }
}