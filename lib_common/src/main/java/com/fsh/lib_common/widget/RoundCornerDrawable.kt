package com.fsh.lib_common.widget

import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap

class RoundCornerDrawable(private val corner:Int,drawable: Drawable) : Drawable() {
    private val mBitmap:Bitmap = drawable.toBitmap()
    private val mPaint:Paint = Paint().also {
        it.isAntiAlias = true
        it.shader = BitmapShader(mBitmap,Shader.TileMode.CLAMP,Shader.TileMode.CLAMP)
    }
    private var rectF:RectF = RectF()

    override fun setBounds(bounds: Rect) {
        super.setBounds(bounds)
        rectF = RectF(bounds)
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        rectF.set(left.toFloat(),top.toFloat(),right.toFloat(),bottom.toFloat())
    }

    override fun getIntrinsicHeight(): Int {
        return mBitmap.height
    }

    override fun getIntrinsicWidth(): Int {
        return mBitmap.width
    }

    override fun draw(canvas: Canvas) {
        canvas.drawRoundRect(rectF,corner.toFloat(),corner.toFloat(),mPaint)
    }

    override fun setAlpha(alpha: Int) {
        mPaint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        mPaint.colorFilter = colorFilter
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}