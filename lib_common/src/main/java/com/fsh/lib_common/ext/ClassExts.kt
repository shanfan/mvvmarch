package com.fsh.lib_common.ext

import android.util.Log
import java.lang.reflect.ParameterizedType
import java.lang.reflect.TypeVariable

fun Class<*>.findParameterizedType(clazz: Class<*>): ParameterizedType?{
    if(clazz.genericSuperclass is ParameterizedType){
        return clazz.genericSuperclass as ParameterizedType
    }
    if(clazz == Object::class.java || clazz.superclass == Object::class.java){
        return null
    }
    return findParameterizedType(clazz.superclass)
}



fun <T> Class<*>.findParameterizedType(clazz: Class<*>,type:Class<T>):Class<T>?{
    if(clazz == Object::class.java || clazz.superclass == Object::class.java){
        return null
    }
    val pType = javaClass.findParameterizedType(clazz) ?: return null
    pType.actualTypeArguments.forEach {
        var clazzType:Class<*>? = null
        if(it is TypeVariable<*>){
            it.bounds.forEach {classType->
                if(type != classType && type.isAssignableFrom(classType as Class<*>)){
                    clazzType = classType
                }
                Log.d("ClassExt","findParameterizedType TypeVariable $it")
            }
            if(clazzType != null){
                return clazzType as Class<T>
            }
        }else{
            clazzType = it as Class<*>
            if(clazzType != type && type.isAssignableFrom(clazzType)){
                return clazzType as Class<T>
            }
        }
    }
    return findParameterizedType(clazz.superclass,type)
}
