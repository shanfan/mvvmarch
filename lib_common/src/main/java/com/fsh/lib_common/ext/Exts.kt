package com.fsh.lib_common.ext

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
suspend fun tryCatch(
    tryBlock: suspend () -> Unit,
    catchBlock: suspend CoroutineScope.(e: Throwable) -> Unit,
    finallyBlock: suspend CoroutineScope.()->Unit
) {
    coroutineScope {
        try{
            tryBlock()
        }catch (e:Throwable){
            catchBlock(e)
        }finally {
            finallyBlock()
        }
    }
}

fun Context.dp2px(dp:Int):Int{
    return (resources.displayMetrics.density * dp +0.5F).toInt()
}

fun Context.sp2px(sp:Int):Int{
    return (resources.displayMetrics.scaledDensity*sp +0.5).toInt()
}

fun Context.screenWidth():Int{
    return resources.displayMetrics.widthPixels
}

fun Context.screenHeight():Int{
    return resources.displayMetrics.heightPixels
}