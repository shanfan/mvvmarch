package com.fsh.lib_common.ext

import android.graphics.Matrix
import android.widget.ImageView

fun ImageView.getMatrixByReflect(): Matrix?{
    return try{
        val declaredField = javaClass.getDeclaredField("mDrawMatrix")
        declaredField.isAccessible = true
        declaredField.get(this) as Matrix
    }catch (e:Throwable){
        null
    }
}