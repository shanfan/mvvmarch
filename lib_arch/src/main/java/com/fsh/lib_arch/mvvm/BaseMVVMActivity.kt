package com.fsh.lib_arch.mvvm

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.R
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.ext.findViewModelType
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.Exception

abstract class BaseMVVMActivity<VDB: ViewBinding,VM: BaseViewModel> : BaseViewBindingActivity<VDB>() ,BaseUIEventHandler {
    protected lateinit var mViewModel:VM
    private var viewModelIsInitialized:Boolean = false
    override var loadingDialog: AlertDialog? = null
        get() = MaterialAlertDialogBuilder(provideContext())
            .setCancelable(false)
            .setView(R.layout.layout_alt_loading)
            .create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(!viewModelIsInitialized){
            throw MVVMException("ViewDataBinding not Initialized")
        }
        registerUIObserver()
    }

    override fun initOtherParamsTypedClasses(clazz: Class<*>) {
        try{
            val viewModelType = clazz.findViewModelType() ?: throw MVVMException("$clazz is not support MVVM")
            try{
                mViewModel = ViewModelProvider(this).get(viewModelType as Class<VM>)
                viewModelIsInitialized = true
            }catch (e: Exception){
                throw MVVMException("init ViewModel $viewModelType fail",e)
            }
        }catch (e:Exception){
            throw MVVMException("init ViewModel fail",e)
        }
    }

    override fun provideContext(): Context  = this
    override fun provideViewModel(): BaseViewModel = mViewModel
    override fun provideLifecycleOwner(): LifecycleOwner = this

}