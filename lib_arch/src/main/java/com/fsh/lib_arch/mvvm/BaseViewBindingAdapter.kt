package com.fsh.lib_arch.mvvm

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_common.ext.findParameterizedType

abstract class BaseViewBindingAdapter<T:ViewBinding> : RecyclerView.Adapter<ViewBindingViewHolder<out ViewBinding>>() {

    override  fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewBindingViewHolder<out ViewBinding> {
        val clazz =
            javaClass.findParameterizedType(javaClass, ViewBinding::class.java) ?: throw MVVMException("can't get ViewBinding class")
        val method = clazz.getMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
        val binding = method.invoke(null,LayoutInflater.from(parent.context),parent,false) as T
        return ViewBindingViewHolder(binding)
    }
}