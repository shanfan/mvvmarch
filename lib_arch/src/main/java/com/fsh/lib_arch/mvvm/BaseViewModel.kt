package com.fsh.lib_arch.mvvm

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fsh.lib_arch.event.SingleLiveEvent
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_common.ext.tryCatch
import com.google.gson.JsonParseException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.json.JSONException
import retrofit2.HttpException
import java.lang.IllegalArgumentException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.ParseException

abstract class BaseViewModel : ViewModel() , LifecycleObserver{

    val uiEvents:UIEvents by lazy { UIEvents() }

    inner class UIEvents{
        val showDialog by lazy { SingleLiveEvent<String>() }
        val dismissDialog by lazy { SingleLiveEvent<Void>() }
        val toastEvent by lazy { SingleLiveEvent<String>() }
    }

    protected fun request(requestBlock:suspend ()->Unit){
        viewModelScope.launch(Dispatchers.IO){
            tryCatch(requestBlock,{e->
                uiEvents.toastEvent.postValue(handleRequestFail(e))
            },{
                uiEvents.dismissDialog.call()
            })
        }
    }

    private fun handleRequestFail(e:Throwable):String{
        return if(e is SocketTimeoutException || e is ConnectException || e is HttpException){
            "网络链接异常"
        }else if(e is JsonParseException || e is JSONException || e is ParseException){
            "数据解析异常"
        }else if(e is UnknownHostException){
            "网络链接异常"
        }else if(e is IllegalArgumentException){
            "参数错误"
        }else if(e is MVVMException){
          e.message ?: ""
        } else{
            "未知错误，可能抛锚了吧~"
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}