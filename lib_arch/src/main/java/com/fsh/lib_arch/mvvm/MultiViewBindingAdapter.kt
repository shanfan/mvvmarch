package com.fsh.lib_arch.mvvm

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class MultiViewBindingAdapter : RecyclerView.Adapter<ViewBindingViewHolder<out ViewBinding>>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewBindingViewHolder<out ViewBinding> {
        return createViewHolderByViewType(parent,viewType)
    }

    private inline fun <reified T:ViewBinding> createViewHolderByViewType(parent: ViewGroup,
                                                                          viewType: Int):ViewBindingViewHolder<T>{
        val method = T::class.java.getMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
        val binding = method.invoke(LayoutInflater.from(parent.context), parent, false) as T
        return ViewBindingViewHolder(binding)
    }

    override fun getItemViewType(position: Int): Int {
        throw IllegalStateException("sub class must override this")
    }
}