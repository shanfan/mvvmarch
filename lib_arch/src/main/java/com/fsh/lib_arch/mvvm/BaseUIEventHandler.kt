package com.fsh.lib_arch.mvvm

import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.fsh.lib_arch.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * 默认界面效果处理
 */
interface BaseUIEventHandler {
    var loadingDialog:AlertDialog?
    /**
     * 获取ViewModel
     */
    fun provideViewModel():BaseViewModel

    /**
     * 获取上下文
     */
    fun provideContext():Context

    fun provideLifecycleOwner(): LifecycleOwner

    fun registerUIObserver(){
        val viewModel = provideViewModel()
        viewModel.uiEvents.showDialog.observe(provideLifecycleOwner(), Observer {
            showLoadingDialog()
        })

        viewModel.uiEvents.dismissDialog.observe(provideLifecycleOwner(), Observer {
            dismissLoadingDialog()
        })
        viewModel.uiEvents.toastEvent.observe(provideLifecycleOwner(), Observer {
            showToast(it)
        })
    }

    fun showLoadingDialog(){
        if(loadingDialog?.isShowing == true){
            loadingDialog?.dismiss()
        }
        if(loadingDialog == null){
            loadingDialog = MaterialAlertDialogBuilder(provideContext())
                .setCancelable(false)
                .setView(R.layout.layout_alt_loading)
                .create()
        }
        loadingDialog?.show()
    }

    fun dismissLoadingDialog(){
        loadingDialog?.dismiss()
    }

    fun showToast(msg:String){
        Toast.makeText(provideContext(),msg,Toast.LENGTH_SHORT).show()
    }
}