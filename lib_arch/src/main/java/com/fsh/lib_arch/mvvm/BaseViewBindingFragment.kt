package com.fsh.lib_arch.mvvm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.ext.findViewBindingType
import java.lang.Exception
import java.lang.reflect.Method

abstract class BaseViewBindingFragment<VDB:ViewBinding> : Fragment(){


    protected lateinit var mViewDataBinding: VDB
    private var isFirst:Boolean = true
    private lateinit var inflateMethod:Method

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initParameterizedTypeClasses()
        Log.d(javaClass.simpleName,"onCreate")
    }

    private fun initParameterizedTypeClasses(){
        var inflateMethodIsInitialized = false
        try{
            val viewBindingClazz = javaClass.findViewBindingType(javaClass)
                ?: throw MVVMException("$javaClass can't support viewBinding")
            inflateMethod = viewBindingClazz.getDeclaredMethod("inflate",LayoutInflater::class.java,ViewGroup::class.java,Boolean::class.java)
            inflateMethodIsInitialized = true
        }catch (e:NoSuchMethodException){
            throw MVVMException("not found inflate(${LayoutInflater::class.java}) method in generate viewBinding class",e)
        }catch (e:SecurityException){
            throw MVVMException("inflate method cannot access",e)
        }catch (e:Exception){
            throw MVVMException("init viewBinding fail",e)
        }
        if(!inflateMethodIsInitialized){
            throw MVVMException("ViewDataBinding not Initialized")
        }
        initOtherParameterizedTypeClasses(javaClass)
    }

    protected open fun initOtherParameterizedTypeClasses(clazz: Class<*>){}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(javaClass.simpleName,"onCreateView")
        try{
            mViewDataBinding = inflateMethod.invoke(null,inflater,container,false) as VDB
            return mViewDataBinding.root
        }catch (e:Exception){
            throw MVVMException("$javaClass create view fail",e)
        }
    }

    private fun onVisible(){
        if(lifecycle.currentState == Lifecycle.State.STARTED && isFirst){
            isFirst = false
            lazyLoadData()
        }
    }

    /**
     * 懒加载数据
     */
    protected fun lazyLoadData(){}



    override fun onResume() {
        super.onResume()
        onVisible()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onVisible()
        Log.d(javaClass.simpleName,"onViewCreated")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(javaClass.simpleName,"onDestroyView")
    }
}