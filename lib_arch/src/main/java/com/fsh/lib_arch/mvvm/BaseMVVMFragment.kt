package com.fsh.lib_arch.mvvm

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.R
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.ext.findViewModelType
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.Exception

abstract class BaseMVVMFragment<VDB: ViewBinding,VM:BaseViewModel> : BaseViewBindingFragment<VDB>(), BaseUIEventHandler{
    override var loadingDialog: AlertDialog? = null
        get() = MaterialAlertDialogBuilder(provideContext())
            .setCancelable(false)
            .setView(R.layout.layout_alt_loading)
            .create()
    protected lateinit var mViewModel:VM
    private var viewModelIsInitialized:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(!viewModelIsInitialized){
            throw MVVMException("ViewModel not initialized")
        }
    }

    override fun initOtherParameterizedTypeClasses(clazz: Class<*>) {
        try{
            val viewModelType = clazz.findViewModelType() ?: throw MVVMException("$clazz is not support MVVM")
            try{
                mViewModel = ViewModelProvider(this).get(viewModelType as Class<VM>)
                viewModelIsInitialized = true
            }catch (e: Exception){
                throw MVVMException("init ViewModel $viewModelType fail",e)
            }
        }catch (e:Exception){
            throw MVVMException("init ViewModel fail",e)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(mViewModel)
        registerUIObserver()
    }

    override fun provideContext(): Context = requireContext()
    override fun provideLifecycleOwner(): LifecycleOwner = this
    override fun provideViewModel(): BaseViewModel = mViewModel
}