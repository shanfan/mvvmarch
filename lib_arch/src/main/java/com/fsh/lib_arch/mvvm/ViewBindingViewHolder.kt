package com.fsh.lib_arch.mvvm

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

class ViewBindingViewHolder<T:ViewBinding>(val mViewBinding:T) : RecyclerView.ViewHolder(mViewBinding.root) {
}