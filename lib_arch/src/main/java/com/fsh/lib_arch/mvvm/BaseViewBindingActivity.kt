package com.fsh.lib_arch.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.ext.findViewBindingType
import java.lang.Exception

abstract class BaseViewBindingActivity<VDB:ViewBinding> : AppCompatActivity() {

    protected lateinit var mViewDataBinding:VDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initParamsTypedClasses()
    }

    private fun initParamsTypedClasses(){
        var viewDataBindingIsInitialized = false
        try{
            val viewBindingClazz = javaClass.findViewBindingType(javaClass)
                ?: throw MVVMException("$javaClass can't support viewBinding")
            mViewDataBinding = viewBindingClazz.getDeclaredMethod("inflate",LayoutInflater::class.java)
                .invoke(null,layoutInflater) as VDB
            setContentView(mViewDataBinding.root)
            viewDataBindingIsInitialized = true
        }catch (e:NoSuchMethodException){
            throw MVVMException("not found inflate(${LayoutInflater::class.java}) method in generate viewBinding class",e)
        }catch (e:SecurityException){
            throw MVVMException("inflate method cannot access",e)
        }catch (e:Exception){
            throw MVVMException("init viewBinding fail",e)
        }
        if(!viewDataBindingIsInitialized){
            throw MVVMException("ViewDataBinding not Initialized")
        }
        initOtherParamsTypedClasses(javaClass)
    }

    protected open fun initOtherParamsTypedClasses(clazz: Class<*>){}
}