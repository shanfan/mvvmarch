package com.fsh.lib_arch.ext

import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.mvvm.BaseViewModel
import com.fsh.lib_common.ext.findParameterizedType

fun Class<*>.findViewBindingType(clazz: Class<*>):Class<out ViewBinding>?{
    return findParameterizedType(this, ViewBinding::class.java)
}


fun Class<*>.findViewModelType():Class<out BaseViewModel>?{
    return findParameterizedType(this, BaseViewModel::class.java)
}