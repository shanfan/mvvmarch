package com.fsh.lib_arch.exception

class MVVMException : Exception{

    constructor(cause:Throwable):super(cause)

    constructor(message:String,cause: Throwable):super(message,cause)

    constructor(message:String):super(message)
}