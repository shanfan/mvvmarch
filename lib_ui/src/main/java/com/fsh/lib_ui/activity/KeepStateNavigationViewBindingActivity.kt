package com.fsh.lib_ui.activity

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.mvvm.BaseViewBindingActivity
import com.fsh.lib_ui.R
import com.fsh.lib_ui.ext.addKeepStateNavigator

abstract class KeepStateNavigationViewBindingActivity<VB:ViewBinding> : BaseViewBindingActivity<VB>() {
    protected abstract val graphId:Int
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navController = findNavController(R.id.fragment)
        val hostFragment = supportFragmentManager.findFragmentById(R.id.fragment)
            ?: throw MVVMException("not found HostFragment[R.id.fragment] in container")
        navController.addKeepStateNavigator(this,hostFragment.childFragmentManager, R.id.fragment)
        navController.setGraph(graphId)
    }

    override fun onBackPressed() {
        val hostFragment = supportFragmentManager.findFragmentById(R.id.fragment) ?: throw MVVMException("not found HostFragment[R.id.fragment] in container")
        if(hostFragment.childFragmentManager.fragments.size > 1){
            super.onBackPressed()
        }else{
            finishAffinity()
            finish()
        }
    }
}