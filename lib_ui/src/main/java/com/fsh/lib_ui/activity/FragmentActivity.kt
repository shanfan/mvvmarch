package com.fsh.lib_ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.FragmentTransaction
import androidx.viewbinding.ViewBinding
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.mvvm.BaseViewBindingActivity
import com.fsh.lib_ui.R
import com.fsh.lib_ui.databinding.ActivityFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FragmentActivity : BaseViewBindingActivity<ActivityFragmentBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(mViewDataBinding.toolbarLayout.toolbar)
        supportActionBar?.title = intent.getStringExtra(EXTRA_TITLE)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mViewDataBinding.toolbarLayout.toolbar.setNavigationOnClickListener { finish() }
        val fragmentName = intent.getStringExtra(EXTRA_FRAGMENT_NAME) ?: throw MVVMException("fragment name can't be null")
        val arguments = intent.getBundleExtra(EXTRA_ARGUMENTS)
        setupFragment(fragmentName,arguments)
    }

    private fun setupFragment(fragmentName: String,arguments: Bundle?){
        val fragment = supportFragmentManager.fragmentFactory.instantiate(classLoader,fragmentName)
        fragment.arguments = arguments
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .replace(mViewDataBinding.layoutContainer.root.id,fragment)
            .commitAllowingStateLoss()
    }

    companion object{
        private const val EXTRA_FRAGMENT_NAME = "fragment_name"
        private const val EXTRA_TITLE = "title"
        private const val EXTRA_ARGUMENTS = "arguments"

        fun startWithFragment(context: Context,fragmentName:String,args:Bundle?,resultTo:Fragment?,requestCode:Int,title:String?){
            val intent = generateStartIntent(context,fragmentName,args, title)
            if(resultTo == null){
                context.startActivity(intent)
            }else{
                resultTo.startActivityForResult(intent,requestCode)
            }
        }

        private fun generateStartIntent(context: Context,fragmentName: String,args: Bundle?,title: String?):Intent{
            val intent = Intent(Intent.ACTION_MAIN)
            intent.setClass(context,FragmentActivity::class.java)
            intent.putExtra(EXTRA_ARGUMENTS,args)
            intent.putExtra(EXTRA_TITLE,title)
            intent.putExtra(EXTRA_FRAGMENT_NAME,fragmentName)
            return intent
        }
    }
}