package com.fsh.lib_ui.activity

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.navigation.findNavController
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.mvvm.BaseMVVMActivity
import com.fsh.lib_arch.mvvm.BaseViewBindingActivity
import com.fsh.lib_arch.mvvm.BaseViewModel
import com.fsh.lib_ui.R
import com.fsh.lib_ui.ext.addKeepStateNavigator

abstract class KeepStateNavigationMVVMActivity<VDB: ViewDataBinding,VM: BaseViewModel> : BaseMVVMActivity<VDB, VM>() {

    abstract  val graphId:Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navController = findNavController(R.id.fragment)
        val hostFragment = supportFragmentManager.findFragmentById(R.id.fragment)
            ?: throw MVVMException("not found HostFragment[R.id.fragment] in container")
        navController.addKeepStateNavigator(this,hostFragment.childFragmentManager,R.id.fragment)
        navController.setGraph(graphId)
    }
}