package com.fsh.lib_ui.ext

import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView


fun BottomNavigationView.indexMenu(menu: MenuItem):Int{
    for(index in 0 until childCount){
        val child = getChildAt(index)
        if(menu.itemId == child.id){
            return index
        }
    }
    return -1
}