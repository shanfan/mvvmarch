package com.fsh.lib_ui.ext

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.annotation.IdRes
import androidx.annotation.Nullable
import androidx.fragment.app.FragmentManager
import androidx.navigation.*
import com.fsh.lib_ui.navigation.KeepStateNavigator
import java.lang.Exception

/**
 * 判断NavigatorProvider是否已经注册了该类型的Navigator
 */
fun NavigatorProvider.hasNavigator(name:String):Boolean{
    return try{
        val declaredField = javaClass.getDeclaredField("mNavigators")
        declaredField.isAccessible = true
        val map = declaredField.get(this) as Map<*, *>
        map.containsKey(name)
    }catch (e:Exception){
        false
    }

}

/**
 * 判断NavigatorProvider是否已经注册了该类型的Navigator
 */
fun NavigatorProvider.hasNavigator(clazz:Class<out Navigator<*>>):Boolean{
    return try{
        val declaredField = javaClass.getDeclaredField("sAnnotationNames")
        declaredField.isAccessible = true
        val map = declaredField.get(this) as Map<*, *>
        map.containsKey(clazz)
    }catch (e:Exception){
        false
    }
}

fun NavController.addKeepStateNavigator(context: Context,fragmentManager: FragmentManager,containerId:Int){
    navigatorProvider.addNavigator(KeepStateNavigator(context,fragmentManager,containerId))
}

fun NavController.navigateKeepState(@IdRes resId:Int, @Nullable args: Bundle?=null, navOptions: NavOptions?=null,navigatorExtras:Navigator.Extras?=null){
    if(!navigatorProvider.hasNavigator(KeepStateNavigator::class.java)){
        throw IllegalStateException("KeepStateNavigator not register,please register before")
    }
    navigate(resId,args, navOptions,navigatorExtras)
}