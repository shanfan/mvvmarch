package com.fsh.lib_ui.navigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigator

/**
 * Copy from [https://github.com/GG811/KeepStateNavigator/blob/master/demo/app/src/main/java/cn/navigation/app/KeepStateNavigator.kt]
 *
 */
@Navigator.Name("keep_state_fragment")
class KeepStateNavigator(
    private val context: Context,
    private val fragmentManager: FragmentManager,
    private val containerId: Int
) : FragmentNavigator(context, fragmentManager, containerId) {

    private val TAG = "KeepStateNavigator"

    @ExperimentalStdlibApi
    private val mBackStack = ArrayDeque<Int>()

    @ExperimentalStdlibApi
    override fun navigate(
        destination: Destination,
        args: Bundle?,
        navOptions: NavOptions?,
        navigatorExtras: Navigator.Extras?
    ): NavDestination? {
        if (fragmentManager.isStateSaved) {
            Log.i(
                TAG, "Ignoring navigate() call: FragmentManager has already"
                        + " saved its state"
            )
            return null
        }
        var className = destination.className
        if(className[0] == '.'){
            className = context.packageName + className
        }
        val fragment = fragmentManager.fragmentFactory.instantiate(context.classLoader, className)
        fragment.arguments = args
        val ft = fragmentManager.beginTransaction()
        var enterAnim = navOptions?.enterAnim ?: -1
        var exitAnim = navOptions?.exitAnim ?: -1
        var popEnterAnim = navOptions?.popEnterAnim ?: -1
        var popExitAnim = navOptions?.popExitAnim ?: -1
        if (enterAnim != -1 || exitAnim != -1 || popEnterAnim != -1 || popExitAnim != -1) {
            enterAnim = if (enterAnim != -1) enterAnim else 0
            exitAnim = if (exitAnim != -1) exitAnim else 0
            popEnterAnim = if (popEnterAnim != -1) popEnterAnim else 0
            popExitAnim = if (popExitAnim != -1) popExitAnim else 0
            ft.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim)
        }
        ft.add(containerId, fragment)
        ft.setPrimaryNavigationFragment(fragment)

        @IdRes val destId = destination.id
        val initialNavigation = mBackStack.isEmpty()
        //隐藏栈顶的fragment
        if(initialNavigation && mBackStack.size < fragmentManager.fragments.size){
            ft.hide(fragmentManager.fragments[mBackStack.size - 1])
        }
        // TODO Build first class singleTop behavior for fragments
        val isSingleTopAdd = (navOptions != null && !initialNavigation
                && navOptions.shouldLaunchSingleTop()
                && mBackStack.last() == destId)
        val isAdded = when{
            initialNavigation ->{
                if(fragmentManager.fragments.isNotEmpty()){
                    fragmentManager.fragments.forEach{ft.remove(it)}
                }
                true
            }
            isSingleTopAdd ->{
                if (mBackStack.size > 1) {
                    fragmentManager.popBackStack(
                        generateBackStackName(mBackStack.size, mBackStack.last() ?: 0),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                    val hideFragment =
                        fragmentManager.fragments[fragmentManager.fragments.size - 2]
                    if (hideFragment != null) ft.hide(hideFragment)

                    ft.addToBackStack(generateBackStackName(mBackStack.size, destId))
                } else {
                    ft.remove(fragmentManager.fragments[0])
                }
                false
            }
            else ->{
                ft.addToBackStack(generateBackStackName(mBackStack.size + 1, destId))
                true
            }
        }
        if(navigatorExtras is Extras){
            for((key, value) in navigatorExtras.sharedElements){
                ft.addSharedElement(key, value)
            }
        }
        val currentFragment: Fragment? = fragmentManager.primaryNavigationFragment
        if(isAdded && currentFragment != null){
            ft.hide(currentFragment)
        }
        ft.setReorderingAllowed(true)
        ft.commit()
        return if(isAdded){
            mBackStack.add(destId)
            destination
        }else{
            null
        }
    }

    @ExperimentalStdlibApi
    override fun popBackStack(): Boolean {
        if(mBackStack.isEmpty()){
            return false
        }
        if(fragmentManager.isStateSaved){
            Log.i(
                TAG, "Ignoring popBackStack() call: FragmentManager has already"
                        + " saved its state"
            )
            return false
        }
        fragmentManager.popBackStack(
            generateBackStackName(mBackStack.size,mBackStack.last()),
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
        mBackStack.removeLast()
        return true
    }


    private fun generateBackStackName(backStackIndex: Int, destId: Int):String{
        return "$backStackIndex-$destId"
    }
}