package com.fsh.lib_refresh

import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fsh.lib_refresh.vh.BaseViewHolder
import com.fsh.lib_refresh.vh.FooterView

abstract class BaseAdapter<T>(val list:List<T>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var headerView:BaseViewHolder<*>? = null
    private var footerView:View? = null

    private var isLoadingMoreEnabled:Boolean = true

    private var onRefreshListener:OnRefreshListener? = null
    var onLoadingMoreListener:OnLoadingMoreListener? = null
    var onItemClickListener:OnItemClickListener? = null

    private var currentState:State = State.INIT

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.addOnScrollListener(RecyclerViewOnScrollerListener())
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if(holder is FooterView){
            Log.d("BaseAdapter","onViewAttachedToWindow FootView $currentState")
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
        if(viewType == VIEW_FOOTER){
            if(footerView == null){
                footerView = LayoutInflater.from(parent.context).inflate(R.layout.layout_refresh,parent,false)
            }
            Log.d("BaseAdapter","createFooterView")
            return FooterView(footerView!!)
        }
        return onCreateContentViewHolder(parent,viewType)
    }

    abstract fun onCreateContentViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(position)
        }
        when(holder){
            is FooterView -> onBindFootViewHolder(holder,position)
            else -> {
                onBindContentViewHolder(holder,position)
            }
        }
    }

    private fun onBindFootViewHolder(footerView: FooterView,position: Int){
        Log.d("BaseAdapter","onBindFootViewHolder $currentState")
        when(currentState){
            State.LOADING -> footerView.onLoading()
            State.LOADING_COMPLETE -> footerView.onLoadingComplete()
            State.LOADING_END -> footerView.onLoadingEnd()
        }
    }

    abstract fun onBindContentViewHolder(holder: RecyclerView.ViewHolder, position: Int)

    override fun getItemViewType(position: Int): Int {
        if(position == itemCount-1){
            return VIEW_FOOTER
        }
        return VIEW_NORMAL
    }


    override fun getItemCount(): Int {
        var count = 1
        count += list.size
        return count
    }

    inner class RecyclerViewOnScrollerListener : RecyclerView.OnScrollListener(){
        //是否是正在向上划
        private var isSlidingUpward:Boolean = false
        private var scrollState:ScrollDirection = ScrollDirection.NONE
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            when{
                dx > 0 -> scrollState = ScrollDirection.LEFT
                dx < 0 -> scrollState = ScrollDirection.RIGHT
                dy > 0 -> scrollState = ScrollDirection.UP
                dy < 0 -> scrollState = ScrollDirection.DOWN
                else -> scrollState = ScrollDirection.NONE
            }
        }

        @RequiresApi(Build.VERSION_CODES.KITKAT)
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if(newState == RecyclerView.SCROLL_STATE_IDLE){
                val layoutManager = recyclerView.layoutManager
                when(layoutManager){
                    is LinearLayoutManager -> {
                        val lastPosition =
                            layoutManager.findLastCompletelyVisibleItemPosition()
                        val count = layoutManager.itemCount
                        val vh =
                            recyclerView.findViewHolderForAdapterPosition(lastPosition)
                        if(lastPosition == count-1 && scrollState == ScrollDirection.UP){
                            onLoadingMore()
                        }
                    }
                }
            }
            }

    }

    private fun onLoadingMore(){
        setState(State.LOADING)
        onLoadingMoreListener?.onLoading()
    }

    fun loadingComplete(){
        Log.d("BaseAdapter","itemCount=[1+${list.size}]=$itemCount")
        setState(State.LOADING_COMPLETE)
    }

    private fun setState(state:State){
        if(currentState != state){
            currentState = state
            notifyItemChanged(itemCount)
        }
    }

    fun setLoadMoreView(view:View){
        footerView = view
    }

    companion object{
        private const val VIEW_HEADER = 0
        private const val VIEW_FOOTER = 1
        const val VIEW_NORMAL = 2
    }

    enum class State{
        INIT,LOADING,LOADING_COMPLETE,LOADING_END
    }

    interface OnLoadingMoreListener{
        fun onLoading()
    }

    interface OnRefreshListener{
        fun onRefresh()
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }

    private enum class ScrollDirection{
        UP,DOWN,LEFT,RIGHT,NONE
    }
}