package com.fsh.lib_refresh.vh

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class FooterView(rootView:View):RecyclerView.ViewHolder(rootView) {
//    init {
//        rootView.visibility = View.GONE
//    }
    fun onLoading(){
        itemView.visibility = View.VISIBLE
    }

    fun onLoadingComplete(){
        itemView.visibility = View.GONE
    }

    fun onLoadingEnd(){
        itemView.visibility = View.GONE
    }
}