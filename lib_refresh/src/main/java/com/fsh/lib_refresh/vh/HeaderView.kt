package com.fsh.lib_refresh.vh

import androidx.viewbinding.ViewBinding

interface HeaderView<Biding:ViewBinding> {

    fun viewHolder():BaseViewHolder<Biding>

}