package com.fsh.lib_refresh.vh

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

class BaseViewHolder<Binding: ViewBinding>(val binding:Binding) : RecyclerView.ViewHolder(binding.root) {
}