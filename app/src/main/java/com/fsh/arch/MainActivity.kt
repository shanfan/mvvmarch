package com.fsh.arch

import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.fsh.arch.databinding.ActivityMainBinding
import com.fsh.lib_ui.activity.KeepStateNavigationMVVMActivity
import com.fsh.lib_ui.activity.KeepStateNavigationViewBindingActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : KeepStateNavigationViewBindingActivity<ActivityMainBinding>() {

    override val graphId: Int
        get() = R.navigation.navigation_app

}