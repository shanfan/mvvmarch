package com.fsh.arch.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fsh.lib_arch.exception.MVVMException
import com.fsh.lib_arch.mvvm.BaseViewBindingFragment

class PagerStateAdapter : FragmentStateAdapter {

    var list:List<BaseViewBindingFragment<*>>? = null

    constructor(fragmentActivity: FragmentActivity):super(fragmentActivity)
    constructor(fragment:Fragment):super(fragment)

    constructor(fragmentManager: FragmentManager,lifeCycle: Lifecycle):super(fragmentManager,lifeCycle)

    override fun getItemCount(): Int = list?.size?:0

    override fun createFragment(position: Int): Fragment {
        if(list?.isNullOrEmpty() == true){
            throw MVVMException("list can not be null or empty")
        }
        return list!![position]
    }
}