package com.fsh.arch.viewmodel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.viewModelScope
import com.fsh.arch.ArchApp
import com.fsh.lib_arch.mvvm.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.io.*
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random

class AppViewModel @ViewModelInject constructor() : BaseViewModel() {

    private val okHttp: OkHttpClient by lazy { getOkHttpClient() }
    private val isRunning: AtomicBoolean = AtomicBoolean(false)
    private var call: Call? = null
    fun downloadUrl(url: String) {
        if (url.isEmpty()) {
            return
        }
        execute(url)
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            })
            .connectTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .writeTimeout(60L, TimeUnit.SECONDS)
            .build()
    }

    private fun execute(url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            Log.i("AppViewModel", "start download $url")
            downloadByOkHttp(url)
        }
    }

    private fun downloadByOkHttp(url: String) {
        val request = Request.Builder()
            .url(url)
            .build()
        call = okHttp.newCall(request)
        call?.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                isRunning.compareAndSet(true, false)
                Log.e("AppViewModel", "download fail", e)
            }

            override fun onResponse(call: Call, response: Response) {
                Log.i("AppViewModel", "download accept response ${response.code}")
                if(response.isSuccessful){
                    response.body?.byteStream()?.let {
                        saveFile(it)
                    }
                }else{
                    Log.w("AppViewModel","request fail [${response.code},${response.message}]")
                }


                isRunning.compareAndSet(true, false)
            }

        })
    }

    private fun downloadByHttpUrlConnection(url: String) {
        try {
            val netURL = URL(url)
            download(netURL)
        } catch (e: Exception) {
            Log.e("AppViewModel", "downloadByHttpUrlConnection fail", e)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestory() {
        if ((call?.isCanceled() == false || call?.isExecuted() == false)) {
            try {
                call?.cancel()
                isRunning.compareAndSet(true, false)
            } catch (e: Exception) {

            }
        }
    }

    override fun onCleared() {
        onDestory()
    }

    private fun download(url: URL) {
        val connection = url.openConnection() as HttpURLConnection
        connection.requestMethod = "GET"
        connection.connectTimeout = 30 * 1000
        connection.readTimeout = 60 * 1000
        connection.connect()
        val responseCode = connection.responseCode
        val responseMessage = connection.responseMessage
        Log.i("AppViewModel", "response[$responseCode,$responseMessage]")
        if (responseCode == HttpURLConnection.HTTP_OK) {
            saveFile(connection.inputStream)
            connection.disconnect()
        }
    }


    private fun saveFile(input: InputStream) {
        try {
            val filesDir = ArchApp.getInstance().filesDir
            val cacheFile = File(filesDir, "tempFile.jpg")
            cacheFile.deleteOnExit()
            cacheFile.createNewFile()
            val output = BufferedOutputStream(FileOutputStream(cacheFile))
            var inputStream: BufferedInputStream = BufferedInputStream(input)
            var buffer: ByteArray = ByteArray(1024)
            while (inputStream.read(buffer) > 0){
                output.write(buffer)
                buffer = ByteArray(1024)
            }
            output.flush()
            output.close()
            inputStream.close()
            Log.i("AppViewModel","saveFile path ${cacheFile.absolutePath}")
        } catch (e: Exception) {
            Log.e("AppViewModel", "saveFile fail", e)
        }
    }

    fun generateStrings():List<String>{
        val count = Random.nextInt(10,30)
        val list = ArrayList<String>()
        for(index in 0 until count){
            list.add(Random.nextInt(65,91).toChar().toString())
        }
        return list
    }
}

