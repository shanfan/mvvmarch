package com.fsh.arch

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArchApp : Application() {

    companion object{
        private lateinit var instance:ArchApp
        fun getInstance():ArchApp = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}