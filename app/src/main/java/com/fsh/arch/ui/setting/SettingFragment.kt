package com.fsh.arch.ui.setting

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.preference.PreferenceFragmentCompat
import com.fsh.arch.R

class SettingFragment : PreferenceFragmentCompat(){
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_settings,rootKey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("SettingFragment","onViewCreated")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("SettingFragment","onDestroyView")
    }
}