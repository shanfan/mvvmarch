package com.fsh.arch.ui.main

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.fsh.arch.R
import com.fsh.arch.adapters.PagerStateAdapter
import com.fsh.arch.databinding.FragmentMainBinding
import com.fsh.lib_arch.mvvm.BaseViewBindingFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : BaseViewBindingFragment<FragmentMainBinding>() {
    private lateinit var pagerAdapter:PagerStateAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pagerAdapter = PagerStateAdapter(this)
        pagerAdapter.list = listOf(HomeFragment.newInstance(), MineFragment.newInstance())
        mViewDataBinding.viewPager.adapter = pagerAdapter
        mViewDataBinding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                mViewDataBinding.bottomNavigation.menu.getItem(position).isChecked = true
            }
        })
        mViewDataBinding.viewPager.isUserInputEnabled = false
        mViewDataBinding.bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.menu_home -> mViewDataBinding.viewPager.currentItem = 0
                else -> mViewDataBinding.viewPager.currentItem = 1
            }
            true
        }
    }
}