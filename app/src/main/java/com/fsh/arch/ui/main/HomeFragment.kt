package com.fsh.arch.ui.main

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fsh.arch.R
import com.fsh.arch.databinding.FragmentHomeBinding
import com.fsh.arch.viewmodel.AppViewModel
import com.fsh.lib_arch.mvvm.BaseMVVMFragment
import com.fsh.lib_refresh.BaseAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

@AndroidEntryPoint
class HomeFragment : BaseMVVMFragment<FragmentHomeBinding,AppViewModel>() {

    private val list:MutableList<String> = ArrayList()

    companion object{
        @JvmStatic
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mViewDataBinding.btnDownload.setOnClickListener {
//            mViewModel.downloadUrl(mViewDataBinding.etUrl.text.toString())
//            mViewDataBinding.etUrl.setText("")
//        }
        for(index in 65 .. 92){
            list.add(index.toChar().toString())
        }
        val layoutManager =  LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mViewDataBinding.recyclerView.layoutManager = layoutManager
        val adapter = Adapter()
        adapter.onLoadingMoreListener = object : BaseAdapter.OnLoadingMoreListener{
            override fun onLoading() {
                lifecycleScope.launch(Dispatchers.IO){
                    val result = mViewModel.generateStrings()
                    delay(1000)
                    lifecycleScope.launch(Dispatchers.Main){
                        adapter.loadingComplete()
                        val index = list.size
                        list.addAll(result)
                        adapter.notifyItemRangeInserted(index,result.size)
                    }
                }
            }
        }
        adapter.onItemClickListener = object : BaseAdapter.OnItemClickListener{
            override fun onItemClick(position: Int) {
                Log.d("HomeFragment","onItemClick $position")
                if(position in 0 until list.size){
                    list[position] = Random.nextInt(48,65).toChar().toString()
                    adapter.notifyItemChanged(position)
                }
            }
        }
        mViewDataBinding.recyclerView.adapter = adapter
        mViewDataBinding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                Log.i("onScrollStateChanged","$newState")
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.i("onScrolled","$dx $dy ${recyclerView.scrollState}")
            }
        })
    }

    inner class Adapter : BaseAdapter<String>(list){
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
//           return VH(layoutInflater.inflate(R.layout.item_recycler_view,parent,false))
//        }
//
//
//
//        override fun onBindViewHolder(holder: VH, position: Int) {
//            holder.itemView.findViewById<TextView>(R.id.tv_name).text = list[position]
//        }
        override fun onCreateContentViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): RecyclerView.ViewHolder {
            return VH(layoutInflater.inflate(R.layout.item_recycler_view,parent,false))
        }

        override fun onBindContentViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            holder.itemView.findViewById<TextView>(R.id.tv_name).text = list[position]
        }

    }

    class VH(item:View) : RecyclerView.ViewHolder(item)
}