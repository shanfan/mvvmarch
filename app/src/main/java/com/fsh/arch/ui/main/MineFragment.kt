package com.fsh.arch.ui.main

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.fsh.arch.R
import com.fsh.arch.databinding.FragmentMineBinding
import com.fsh.arch.ui.setting.SettingFragment
import com.fsh.arch.viewmodel.AppViewModel
import com.fsh.lib_arch.mvvm.BaseMVVMFragment
import com.fsh.lib_ui.activity.FragmentActivity
import com.fsh.lib_ui.ext.navigateKeepState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MineFragment : BaseMVVMFragment<FragmentMineBinding, AppViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance(): MineFragment {
            val args = Bundle()
            val fragment = MineFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.tvMine.setOnClickListener {
//            view.findNavController().navigateKeepState(R.id.action_setting)
            FragmentActivity.startWithFragment(
                requireContext(),
                SettingFragment::class.java.name,
                null,
                this,
                100,
                getString(R.string.tv_setting)
            )
        }

    }
}