package com.fsh.widgets

import android.R
import android.app.Activity
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.res.Configuration
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import com.fsh.lib_common.ext.dp2px
import java.lang.ref.WeakReference
import kotlin.math.abs

open class KeyboardShowListener(content: View) : FullScreenKeyBoardResizeHelper(content, false) {
    private val contextRef:WeakReference<Context> = WeakReference(content.context)
    private var usableHeightPrevious: Int = 0
    private var landMaxHeight: Int = 0
    private var portraitMaxHeight: Int = 0
    private var showListener: ((isShow: Boolean, keyboardHeight: Int) -> Unit)? = null

    fun setListener(listener: (isShow: Boolean, keyboardHeight: Int) -> Unit) {
        this.showListener = listener
    }

    private var lastShow = false;

    override fun possiblyResizeChildOfContent() {
        val usableHeightNow = computeUsableHeight()
        if (usableHeightNow != usableHeightPrevious) { //如果两次高度不一致
            usableHeightPrevious = usableHeightNow
            val isShow: Boolean
            val keyboardHeight: Int
            if (content.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (landMaxHeight == 0) {
                    val wm = content.context.getSystemService(WINDOW_SERVICE) as WindowManager
                    val dm = DisplayMetrics()
                    wm.defaultDisplay.getMetrics(dm)
                    val height = dm.heightPixels
                    isShow = abs(height - usableHeightNow) > contextRef.get()?.dp2px(100) ?: 100
                    if (!isShow) {
                        landMaxHeight = Math.max(landMaxHeight, usableHeightNow)
                    }
                    keyboardHeight = height - usableHeightNow
                } else {
                    landMaxHeight = Math.max(landMaxHeight, usableHeightNow)
                    isShow = usableHeightNow != landMaxHeight
                    keyboardHeight = landMaxHeight - usableHeightNow
                }
            } else {
                if (portraitMaxHeight == 0) {
                    val wm = content.context.getSystemService(WINDOW_SERVICE) as WindowManager
                    val dm = DisplayMetrics()
                    wm.defaultDisplay.getMetrics(dm)
                    val height = dm.heightPixels
                    isShow = Math.abs(height - usableHeightNow) > contextRef.get()?.dp2px(100) ?: 100
                    if (!isShow) {
                        portraitMaxHeight = Math.max(portraitMaxHeight, usableHeightNow)
                    }
                    keyboardHeight = height - usableHeightNow
                } else {
                    portraitMaxHeight = Math.max(portraitMaxHeight, usableHeightNow)
                    isShow = usableHeightNow != portraitMaxHeight
                    keyboardHeight = portraitMaxHeight - usableHeightNow
                }
            }
            if (isShow != lastShow) {
                lastShow = isShow
                showListener?.invoke(isShow, keyboardHeight)
            }
        }
    }

    companion object {
        @JvmStatic
        fun assistRoot(content: View): KeyboardShowListener {
            return KeyboardShowListener(content)
        }

        @JvmStatic
        fun assistActivity(content: Activity): KeyboardShowListener {
            return KeyboardShowListener(content.findViewById<View>(R.id.content))
        }
    }
}
