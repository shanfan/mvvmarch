package com.fsh.widgets

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import androidx.core.view.doOnDetach
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope

import kotlinx.coroutines.*

/**
 * 这个类用于解决沉浸式状态栏布局全屏时无法resize的问题
 */
open class FullScreenKeyBoardResizeHelper(val content: View, private val clipStatusBar: Boolean = false) {
    private var usableHeightPrevious = 0
    private var listener: OnGlobalLayoutListener? = null
    private var job: Job? = null

    init {
        listener = OnGlobalLayoutListener {
            if (job?.isCancelled == false)
                job?.cancel()
            job = delay(
                100,
                Dispatchers.Main,
                ViewTreeLifecycleOwner.get(content)
            ) { possiblyResizeChildOfContent() }
        }
        if (usableHeightPrevious > 0)
            content.layoutParams.height = usableHeightPrevious
        content.viewTreeObserver.addOnGlobalLayoutListener(listener)
        content.doOnDetach {
            content.viewTreeObserver.removeOnGlobalLayoutListener(listener)
            job = null
            listener = null
        }
    }

    fun reset() {
        if (listener != null) {
            content.viewTreeObserver.removeOnGlobalLayoutListener(listener)
            content.layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
            content.requestLayout() //请求重新布局
            listener = null
        }
    }

    protected open fun possiblyResizeChildOfContent() {
        val usableHeightNow = computeUsableHeight()
        if (usableHeightNow != usableHeightPrevious) { //如果两次高度不一致 //将计算的可视高度设置成视图的高度
            content.layoutParams.height = usableHeightNow //高度恢复时避免直接设值，避免有底导航等影响高度
            content.requestLayout() //请求重新布局
            usableHeightPrevious = usableHeightNow
        }
    }

    protected fun computeUsableHeight(): Int { //计算视图可视高度
        val r = Rect()
        content.getWindowVisibleDisplayFrame(r)
        return r.bottom - if (clipStatusBar) r.top else 0
    }

    companion object {
        fun assistActivity(activity: Activity): FullScreenKeyBoardResizeHelper {
            return FullScreenKeyBoardResizeHelper(activity.findViewById(R.id.content))
        }

        fun assistRoot(
            content: View,
            clipStatusBar: Boolean = false
        ): FullScreenKeyBoardResizeHelper {
            return FullScreenKeyBoardResizeHelper(content, clipStatusBar)
        }
    }

    private fun delay(
        millisecond: Long, dispatcher: CoroutineDispatcher? = null,
        owner: LifecycleOwner? = this as? LifecycleOwner,
        action: () -> Unit
    ): Job {
        return (owner?.lifecycleScope ?: GlobalScope).launch(Dispatchers.Default) {
            delay(millisecond)
            dispatcher?.let {
                (owner?.lifecycleScope ?: GlobalScope).launch(dispatcher) {
                    action.invoke()
                }
            } ?: action.invoke()
        }
    }
}