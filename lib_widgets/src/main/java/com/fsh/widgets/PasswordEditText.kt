package com.fsh.widgets

import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.text.InputFilter
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.addTextChangedListener
import com.fsh.lib_common.ext.dp2px

class PasswordEditText : AppCompatEditText {
    private var mCount: Int = 6
    private var mBorderColor: Int = Color.GRAY
    private var mFocusBorderColor: Int = Color.GREEN
    private var mBoxSize: Int = context.dp2px(36)
    private var mBorderRadius = context.dp2px(3)
    private var mSpaceSize: Int = context.dp2px(16)
    private var mBorderWidth: Int = context.dp2px(1)
    private var mSelectIndex: Int = -1
    private var mBoxRect: RectF = RectF()
    private lateinit var mBoxPaint: Paint
    private lateinit var mTextPaint: Paint
    private var mTextBounds: Rect = Rect()
    private val mInputTypeIsPassword:Boolean
                get() {
                    val variation = inputType and (EditorInfo.TYPE_MASK_CLASS or EditorInfo.TYPE_MASK_VARIATION)
                    return variation == EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_VARIATION_PASSWORD
                }

    constructor(context: Context) : this(context, null) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }


    private fun init(attrs: AttributeSet?) {
        setWillNotDraw(false)
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PasswordEditText)
        mCount = typedArray.getInt(R.styleable.PasswordEditText_count, mCount)
        mBorderColor = typedArray.getColor(R.styleable.PasswordEditText_borderColor, mBorderColor)
        mFocusBorderColor = typedArray.getColor(R.styleable.PasswordEditText_borderFocusColor, mFocusBorderColor)
        mBoxSize = typedArray.getDimensionPixelSize(R.styleable.PasswordEditText_boxSize, mBoxSize)
        mBorderRadius = typedArray.getDimensionPixelSize(R.styleable.PasswordEditText_borderRadius, mBorderRadius)
        mBorderWidth = typedArray.getDimensionPixelSize(R.styleable.PasswordEditText_borderSize, mBorderWidth)
//        typedArray.getColor()
        typedArray.recycle()
        mBoxPaint = Paint().apply {
            style = Paint.Style.STROKE
            strokeWidth = mBorderWidth.toFloat()
        }
        mTextPaint = paint
        mTextPaint.textAlign = Paint.Align.CENTER
        filters = arrayOf(InputFilter.LengthFilter(mCount))
        background = ColorDrawable(Color.TRANSPARENT)
        addTextChangedListener {
            if(it?.length == mCount){
                val input = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                input.hideSoftInputFromWindow(applicationWindowToken,0)
                clearFocus()
            }
        }
        //判断输入数据类型是否是数字或者数字密码
        checkInputType(inputType)
        //禁止长按复制
        isLongClickable = false
        setTextIsSelectable(false)
        isCursorVisible = false
    }

    override fun setInputType(type: Int) {
        checkInputType(type)
        super.setInputType(type)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(getWidthSize(widthMeasureSpec), getHeightSize(heightMeasureSpec))
    }

    private fun getWidthSize(widthMeasureSpec: Int): Int {
        var size = MeasureSpec.getSize(widthMeasureSpec)
        val mode = MeasureSpec.getMode(widthMeasureSpec)
        when (mode) {
            MeasureSpec.UNSPECIFIED -> {
                size = paddingLeft + paddingRight + mCount * mBoxSize + (mCount - 1) * mSpaceSize
            }
            else -> {
                val width = paddingLeft + paddingRight + mCount * mBoxSize;
                mSpaceSize = (size - width)/(mCount-1)
            }
        }
        return size
    }

    private fun getHeightSize(heightMeasureSpec: Int): Int {
        return paddingBottom + paddingTop + mBoxSize
    }

    override fun onDraw(canvas: Canvas) {
        mSelectIndex = (text?.length ?: 0) - 1
        if(mSelectIndex == -1 && hasFocus() && text?.isEmpty() == true){
            mSelectIndex = 0
        }
        if(!hasFocus()){
            mSelectIndex = -1
        }
        drawBox(canvas)
    }

    private fun drawBox(canvas: Canvas) {
        for (index in 0 until mCount) {
            calculateBoxRect(index)
            if (index == mSelectIndex) {
                mBoxPaint.color = mFocusBorderColor
            } else {
                mBoxPaint.color = mBorderColor
            }
            canvas.drawRoundRect(mBoxRect, mBorderRadius.toFloat(), mBorderRadius.toFloat(), mBoxPaint)
            drawText(canvas, index)
        }
    }

    private fun calculateBoxRect(index: Int) {
        val left = paddingLeft + index * (mSpaceSize + mBoxSize).toFloat()
        val top = paddingTop.toFloat()
        val right = left + mBoxSize
        val bottom = top + mBoxSize
        mBoxRect.set(left, top, right, bottom)
    }

    private fun drawText(canvas: Canvas, index: Int) {
        val char = getCharAt(index) ?: return
        mTextPaint.getTextBounds(char.toString(), 0, 1, mTextBounds)
        val fontMetrics = mTextPaint.fontMetrics

        val y =  (mBoxRect.height() - fontMetrics.bottom - fontMetrics.top) / 2 + fontMetrics.bottom
        canvas.drawText(char.toString(), 0, 1, mBoxRect.centerX(), y, mTextPaint)
    }

    private fun getCharAt(index: Int): Char? {
        val text = text ?: return null
        if (index !in text.indices) {
            return null
        }
        return if (mInputTypeIsPassword) {
            return '*'
        } else text[index]
    }

    private fun checkInputType(inputType:Int){
        val variation = inputType and (EditorInfo.TYPE_MASK_CLASS or EditorInfo.TYPE_MASK_VARIATION)
        val numberPasswordInputType = (variation
                == EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_VARIATION_PASSWORD)
        val flag = EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_FLAG_SIGNED
        val isNumber = (variation == flag || inputType == flag)
        if(!numberPasswordInputType && !isNumber){
            throw IllegalArgumentException("input type only support numberSigned and numberPassword, inputType=$inputType")
        }
    }

}