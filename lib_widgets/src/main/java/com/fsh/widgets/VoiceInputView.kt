package com.fsh.widgets

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.WindowManager
import androidx.core.graphics.contains
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.toPoint
import com.fsh.lib_common.ext.dp2px
import com.fsh.lib_common.ext.screenWidth
import com.fsh.lib_common.ext.sp2px
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sqrt

class VoiceInputView : View {

    private lateinit var voiceInputIcon:Drawable
    private var pressedColor:Int = Color.blue(90)
    private var shadowColor:Int = Color.blue(200)
    private lateinit var cancelIcon:Drawable
    private var cancelText:String = ""
    private var cancelPadding:Int = context.dp2px(10)

    private var voiceIconColor:Int = Color.BLACK
    private var voiceIconPressedColor:Int = Color.WHITE

    private var cancelIconColor:Int = Color.BLACK
    private var cancelIconPressedColor:Int = Color.RED

    private var cancelTextColor:Int = Color.GRAY
    private var cancelTextPressedColor:Int = Color.RED


    private var voiceIconSize:Int = 0
    private var voiceIconPadding:Int =0
    private var cancelTextSize:Int = 0
    private var cancelIconSize:Int = 0
    private var cancelAreaMargin:Int = context.dp2px(13)

    private var cancelTextPaint:Paint
    private var drawablePaint:Paint
    private var wavePaint:Paint

    private var currentState: State = State.NONE

    private val inputArea:Rect = Rect()
    private val cancelArea:Rect = Rect()

    private var textBounds:Rect = Rect()
    private var preTouchPoint:PointF = PointF()
    private val touchSlop = ViewConfiguration.get(context).scaledTouchSlop
    var onInputListener: OnInputListener? = null
    private var pressedStartTime:Long = 0
    private val SLOP_TIME = 1000L

    constructor(context: Context?) : this(context,null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs,0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ){
        context?.obtainStyledAttributes(attrs,R.styleable.VoiceInputView)?.let {
            try{
                voiceInputIcon = it.getDrawable(R.styleable.VoiceInputView_voiceIcon) ?: throw IllegalStateException("voice icon can't be null,please set voiceIcon attribute")
                cancelIcon = it.getDrawable(R.styleable.VoiceInputView_cancelIcon) ?: throw IllegalStateException("voice icon can't be null,please set voiceIcon attribute")
                cancelText = it.getString(R.styleable.VoiceInputView_cancelText) ?: ""

                pressedColor = it.getColor(R.styleable.VoiceInputView_pressedColor,pressedColor)
                shadowColor = it.getColor(R.styleable.VoiceInputView_shadowColor,shadowColor)

                voiceIconColor = it.getColor(R.styleable.VoiceInputView_voiceIconColor,voiceIconColor)
                voiceIconPressedColor = it.getColor(R.styleable.VoiceInputView_voiceIconPressedColor,voiceIconPressedColor)

                cancelIconColor = it.getColor(R.styleable.VoiceInputView_cancelIconColor,cancelIconColor)
                cancelIconPressedColor = it.getColor(R.styleable.VoiceInputView_cancelIconPressedColor,cancelIconPressedColor)

                cancelTextColor = it.getColor(R.styleable.VoiceInputView_cancelTextColor,cancelTextColor)
                cancelTextPressedColor = it.getColor(R.styleable.VoiceInputView_cancelTextPressedColor,cancelTextPressedColor)

                voiceIconSize = max(voiceInputIcon.intrinsicWidth,voiceInputIcon.intrinsicHeight)
                voiceIconPadding = it.getDimensionPixelSize(R.styleable.VoiceInputView_voiceIconPadding,context.dp2px(35))
                cancelIconSize = max(cancelIcon.intrinsicWidth,cancelIcon.intrinsicHeight)
                cancelTextSize = it.getDimensionPixelSize(R.styleable.VoiceInputView_cancelTextSize,context.sp2px(12))
                cancelPadding = it.getDimensionPixelSize(R.styleable.VoiceInputView_cancelPadding,cancelPadding)
            }finally {
                it.recycle()
            }
        } ?: throw IllegalStateException("can't obtain styledAttributes")
        //取消文本的画笔初始化
        cancelTextPaint = Paint().also {
            it.isAntiAlias = true
            it.textSize = cancelTextSize.toFloat()
            it.color = cancelTextColor
        }
        drawablePaint = Paint().also {
            it.isAntiAlias = true
        }
        wavePaint = Paint().also {
            it.isAntiAlias = true
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(getWidthSizeWhenMeasure(widthMeasureSpec),getHeightSizeWhenMeasure(heightMeasureSpec))
    }

    private fun getWidthSizeWhenMeasure(measureSpec:Int):Int{
        val mode = MeasureSpec.getMode(measureSpec)
        var size = MeasureSpec.getSize(measureSpec)
        when(mode){
            MeasureSpec.UNSPECIFIED ->{
                val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                size = windowManager.defaultDisplay.width
            }
        }
        return size
    }

    private fun getHeightSizeWhenMeasure(measureSpec :Int):Int{
        var size = 2 * cancelAreaMargin + cancelIconSize
        cancelTextPaint.getTextBounds(cancelText,0,cancelText.length,textBounds)
        size += textBounds.height() + context.dp2px(2)
        size += voiceIconSize + voiceIconPadding*2
        return size
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val areaBottom = cancelAreaMargin*2 + cancelIconSize + textBounds.height() + context.dp2px(2)
        cancelArea.set(0,0,width,areaBottom)

        inputArea.set(0,areaBottom,w,h)
    }

    override fun onDraw(canvas: Canvas) {
        Log.d("VoiceInputView","onDraw currentState=$currentState")
        canvas.drawColor(Color.WHITE)
        if(currentState in State.PRESSED..State.CANCEL){
            drawPressedWave(canvas)
        }
        drawVoiceIcon(canvas)
        if(currentState in State.PRESSED..State.CANCEL){
            drawCancelInfo(canvas)
        }
    }

    /**
     * 绘制语音Icon
     */
    private fun drawVoiceIcon(canvas: Canvas){
        val left = (width- voiceIconSize)/2F
        val top = 2F * cancelAreaMargin + cancelIconSize + textBounds.height() + context.dp2px(2) + voiceIconPadding
        drawBitmap(left,top,voiceIconColor,voiceIconPressedColor,voiceInputIcon,canvas)
    }

    private fun drawBitmap(left:Float,top:Float,color:Int,pressedColor:Int,drawable: Drawable,canvas: Canvas){
        if(currentState == State.NONE){
            drawablePaint.colorFilter = PorterDuffColorFilter(color,PorterDuff.Mode.SRC_ATOP)
        }else{
            drawablePaint.colorFilter = PorterDuffColorFilter(pressedColor,PorterDuff.Mode.SRC_ATOP)
        }
        canvas.drawBitmap(drawable.toBitmap(),left,top,drawablePaint)
    }

    /**
     * 绘制圆弧和波形
     */
    private fun drawPressedWave(canvas: Canvas){

        var top = 2F * cancelAreaMargin + cancelIconSize+ context.dp2px(2)+textBounds.height() +context.dp2px(15)
        var bottom = top + calculateRadius((height-top).toInt())
        wavePaint.color = pressedColor
        canvas.drawArc(0F,top,width.toFloat(),bottom,-180F,180F,true,wavePaint)
        top -= context.dp2px(15)
        bottom = top + calculateRadius((height-top).toInt())
        wavePaint.color = shadowColor
        canvas.drawArc(0F,top,width.toFloat(),bottom,-180F,180F,true,wavePaint)
    }

    private fun calculateRadius(height:Int):Float{
        val halfWidth = context.screenWidth() / 2F
        //地边的长度
        val bottomWidth = sqrt(height*height + halfWidth*halfWidth)
        //cos
        val cos = height*1F/bottomWidth
        val radius = bottomWidth/2F / cos
        return radius
    }

    /**
     * 绘制取消提示信息
     */
    private fun drawCancelInfo(canvas: Canvas){
        var left = (width- voiceIconSize)/2F
        var top = cancelAreaMargin.toFloat()
        val pressedColor = if(currentState == State.PRESSED) cancelIconColor else cancelIconPressedColor
        drawBitmap(left,top,cancelIconColor,pressedColor,cancelIcon,canvas)
        left = (width - textBounds.width())/2F
        top = cancelAreaMargin + cancelIconSize + context.dp2px(2)*1F
        val fontMetrics = cancelTextPaint.fontMetrics
        top += (textBounds.height() - fontMetrics.bottom - fontMetrics.top)/2
        cancelTextPaint.color = if(currentState == State.PRESSED) cancelTextColor else cancelTextPressedColor
        canvas.drawText(cancelText,left,top,cancelTextPaint)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.action){
            MotionEvent.ACTION_DOWN ->{
                preTouchPoint.set(event.x,event.y)
                //只有触摸在输入区域才能算数
                if(inputArea.contains(preTouchPoint.toPoint())){
                    currentState = State.PRESSED
                    pressedStartTime = System.currentTimeMillis()
                    onInputListener?.onPressed()
                    postInvalidate()
                }
            }
            MotionEvent.ACTION_MOVE ->{
                val moveX = abs(preTouchPoint.x - event.x)
                val moveY = abs(preTouchPoint.y - event.y)
                val moveDis = sqrt((moveX*moveX).toDouble()+moveY*moveY).toInt()
                if(moveDis > touchSlop){
                    if((event.y <0F || cancelArea.contains(event.x.toInt(),event.y.toInt()))){
                        if(currentState == State.PRESSED){
                            currentState = State.CANCEL
                            preTouchPoint.set(event.x,event.y)
                            postInvalidate()
                        }
                    }else{
                        preTouchPoint.set(event.x,event.y)
                        currentState = State.PRESSED
                        postInvalidate()
                    }

                }
            }
            MotionEvent.ACTION_UP ->{
                if(currentState == State.CANCEL){
                    onInputListener?.onCancel()
                }else if(currentState == State.PRESSED){
                    //输入时间小于1秒，就不计入
                    if(System.currentTimeMillis() - pressedStartTime < SLOP_TIME){
                        onInputListener?.onCancel()
                    }else{
                        onInputListener?.onComplete()
                    }
                }
                currentState = State.NONE
                preTouchPoint.set(-1F,-1F)
                postInvalidate()
            }
        }
        return true
    }

    /**
     * 取消输入
     * TODO
     */
    fun cancel(){
        currentState = State.CANCEL
        postInvalidate()
        dispatchTouchEvent(MotionEvent.obtain(pressedStartTime,System.currentTimeMillis(),MotionEvent.ACTION_UP,preTouchPoint.x,preTouchPoint.y,0))
        setFocusable(false)
    }

    enum class State{
        /**
         * 初始化，没有任何操作
         */
        NONE,
        /**
         * 点击到控件上
         */
        PRESSED,
        /**
         * 滑动到取消区域
         */
        CANCEL
    }

    interface OnInputListener{
        /**
         * 按压开始
         */
        fun onPressed()

        /**
         * 取消
         */
        fun onCancel()

        /**
         * 输入完成
         */
        fun onComplete()
    }

}