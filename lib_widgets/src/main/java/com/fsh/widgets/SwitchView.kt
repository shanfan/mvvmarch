package com.fsh.widgets

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import androidx.core.animation.addListener
import com.fsh.lib_common.ext.dp2px
import java.lang.Exception
import kotlin.math.max
import kotlin.math.min

/**
 * 开关控件
 */
class SwitchView : View {

    companion object{
        private const val TIME_DELGE = 100L
        private const val DURATION_ANIM = 150L
    }

    private var normalColor:Int = Color.parseColor("#E5597EF7")
    private var checkedColor:Int = Color.parseColor("#E5597EF7")
    private var thumbColor:Int = Color.WHITE
    private var widthSize = context.dp2px(46)
    private var heightSize = context.dp2px(28)
    private var mPaint:Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }
    private var thumbRadius:Float = 0F
    private var pressedTime:Long = 0L
    private var anim:ValueAnimator? = null
    private var animInterpolator:Interpolator = OvershootInterpolator(0.1F)
    private var mOffset:Float = 0F
    private var state:State = State.NOT_CHECKED
    var onChangeChangeListener:OnCheckedChangeListener? = null
    constructor(context: Context?) : this(context,null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs,0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ){
        context?.obtainStyledAttributes(attrs,R.styleable.SwitchView)?.let{
            try{
                normalColor = it.getColor(R.styleable.SwitchView_normalColor,normalColor)
                checkedColor = it.getColor(R.styleable.SwitchView_checkedColor,checkedColor)
                thumbColor = it.getColor(R.styleable.SwitchView_thumbColor,thumbColor)
            }catch (e:Exception){
                it.recycle()
            }
        } ?: throw IllegalStateException("can't obtain styled attributes")
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(getMeasureWidth(widthMeasureSpec),getMeasureHeight(heightMeasureSpec))
    }

    private fun getMeasureWidth(spec:Int):Int{
        var size = MeasureSpec.getSize(spec)
        when(MeasureSpec.getMode(spec)){
            MeasureSpec.UNSPECIFIED ->{
                size = widthSize + paddingLeft + paddingRight
            }
        }
        return size
    }

    private fun getMeasureHeight(spec:Int):Int{
        var size = MeasureSpec.getSize(spec)
        when(MeasureSpec.getMode(spec)){
            MeasureSpec.UNSPECIFIED ->{
                size = heightSize + paddingTop + paddingBottom
            }
        }
        return size
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        widthSize = w
        heightSize = h
        thumbRadius = (min(w,h)-context.dp2px(2))/2F
        mOffset = thumbRadius
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawShapeBackground(it)
            drawThumb(it)
        }
    }

    //绘制背景
    private fun drawShapeBackground(canvas: Canvas){
        val radius = max(heightSize/2F,widthSize/2F)
        //TODO 根据选中和非选中状态切换颜色
        if(state == State.NOT_CHECKED){
            mPaint.color = normalColor
        }else{
            mPaint.color = checkedColor
        }
        canvas.drawRoundRect(
            0F,0F,widthSize.toFloat(),heightSize.toFloat(),radius,radius,mPaint)
    }

    //绘制Thumb
    private fun drawThumb(canvas: Canvas){
        mPaint.color = thumbColor
        val offset = context.dp2px(1) + mOffset
        canvas.drawCircle(offset,heightSize/2F,thumbRadius,mPaint)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(!isEnabled){
            return false
        }
        when(event?.action){
            MotionEvent.ACTION_DOWN -> pressedTime = System.currentTimeMillis()
            MotionEvent.ACTION_MOVE -> {}
            MotionEvent.ACTION_UP -> {
                val takeTime = System.currentTimeMillis() - pressedTime
                Log.d("SwitchView","take time = $takeTime")
                //点击事件为300毫秒
                if(takeTime - pressedTime <= TIME_DELGE){
                    if(state == State.CHECKED){
                        startRightToLeftAnim()
                    }else{
                        startLeftToRightAnim()
                    }
                }
            }
        }
        return true
    }

    private fun startLeftToRightAnim(){
        val start = context.dp2px(1) + thumbRadius
        val end = widthSize - context.dp2px(2) - thumbRadius
        startAnim(start,end)
    }

    private fun startRightToLeftAnim(){
        val start = widthSize - context.dp2px(2) - thumbRadius
        val end = thumbRadius
        startAnim(start,end)
    }

    private fun startAnim(start:Float,end:Float){
        if(anim?.isRunning == true){
            return
        }
        anim = ValueAnimator.ofFloat(start,end)
        anim?.interpolator = animInterpolator
        anim?.duration = DURATION_ANIM
        anim?.addListener(
            onEnd = {
                mOffset = end
                postInvalidate()
                Log.d("SwitchView","Animation end")
                state = if(state == State.NOT_CHECKED) State.CHECKED else State.NOT_CHECKED
                onChangeChangeListener?.onCheckedChanged(state == State.CHECKED)
            }
        )
        anim?.addUpdateListener {
            mOffset = it.animatedValue.toString().toFloat()
            postInvalidate()
        }
        anim?.start()
    }

    private enum class State{
        CHECKED,NOT_CHECKED
    }

    fun setChecked(isChecked: Boolean){
        val stateIsChecked = state == State.CHECKED
        if(stateIsChecked == isChecked){
            return
        }
        if(isChecked){
            startLeftToRightAnim()
        }else{
            startRightToLeftAnim()
        }
    }

    fun isChecked():Boolean = state == State.CHECKED

    interface OnCheckedChangeListener{
        fun onCheckedChanged(isChecked:Boolean)
    }
}