package com.fsh.widgets

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.fsh.lib_common.ext.dp2px
import com.fsh.lib_common.ext.sp2px
import kotlin.math.max

class SearchView : LinearLayout {
    //左边的搜索按钮
    private lateinit var mSearchIcon: ImageView
    //右边的搜索输入框
    private lateinit var mSearchInput: EditText
    private var onSearchCallback: ((input: String) -> Unit)? = null
    private var keyboardListener: KeyboardShowListener? = null
    private var preSearchTime:Long = 0L
    constructor(context: Context) : super(context){
        init(null,0)
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        init(attrs,0)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init(attrs,defStyleAttr)
    }


    private  fun init(attrs: AttributeSet?, defStyleAttr: Int){
        orientation = HORIZONTAL
        val padding12 = context.dp2px(12)
        val padding8 = context.dp2px(8)
        setPadding(padding12,padding8,padding12,padding8)
        gravity = Gravity.CENTER_VERTICAL
        LayoutInflater.from(context).inflate(R.layout.layout_search_view,this)
        mSearchIcon = findViewById(R.id.search_icon)
        mSearchInput = findViewById(R.id.search_input)
        //在失去焦点的时候，判断是否需要显示搜索图标
        mSearchInput.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && mSearchInput.text.isEmpty()) {
                mSearchIcon.visibility = View.VISIBLE
            } else {
                mSearchIcon.visibility = View.GONE
            }
        }
        mSearchInput.setOnEditorActionListener { v, actionId, event ->
            //防抖
            if (actionId == EditorInfo.IME_ACTION_SEARCH && System.currentTimeMillis() - preSearchTime > 400) {
                onSearchCallback?.invoke(v.text.toString())
                preSearchTime = System.currentTimeMillis()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        mSearchInput.clearFocus()
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.SearchView,
            defStyleAttr,
            defStyleAttr
        )
        try {
            initAttrs(typedArray)
        } finally {
            typedArray.recycle()
        }
    }
    private fun initAttrs(typedArray: TypedArray) {
        //设置图片属性
        typedArray.getDrawable(R.styleable.SearchView_icon)?.let {
            val defaultSize = max(it.intrinsicWidth, it.intrinsicHeight)
            val iconSize =
                typedArray.getDimensionPixelSize(R.styleable.SearchView_iconSize, defaultSize)
            val layoutParams = mSearchIcon.layoutParams
            layoutParams.width = iconSize
            layoutParams.height = iconSize
            mSearchIcon.layoutParams = layoutParams
            setIcon(it)
        }
        //设置hint文字
        typedArray.getString(R.styleable.SearchView_android_hint)?.let {
            setHint(it)
        }
        //设置hint文字颜色
        val hintColor =
            typedArray.getColor(R.styleable.SearchView_android_textColorHint, Color.GRAY)
        setTextColorHint(hintColor)
        //设置文字大小
        val textSize = typedArray.getDimensionPixelSize(
            R.styleable.SearchView_android_textSize,
            context.sp2px(16)
        )
        setTextSize(textSize)
        //设置输入文字颜色
        val textColor = typedArray.getColor(R.styleable.SearchView_android_textColor,Color.BLACK)
        setTextColor(textColor)
        //设置背景
        val backgroundColor = typedArray.getDrawable(R.styleable.SearchView_android_background)
        background = backgroundColor
    }

    fun setIcon(@DrawableRes resId: Int) {
        mSearchIcon.setImageResource(resId)
    }

    fun setIcon(drawable: Drawable) {
        mSearchIcon.setImageDrawable(drawable)
    }

    fun setHint(hint: String) {
        mSearchInput.hint = hint
    }

    fun setTextColorHint(@ColorInt color: Int) {
        mSearchInput.setHintTextColor(color)
    }

    fun setTextSize(textSize: Int) {
        mSearchInput.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())
    }

    fun setTextColor(@ColorInt color:Int){
        mSearchInput.setTextColor(color)
    }

    fun setOnSearchCallback(callback: ((input: String) -> Unit)) {
        this.onSearchCallback = callback
    }
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        //添加键盘监听
        keyboardListener = KeyboardShowListener.assistRoot(this)
        keyboardListener?.setListener { isShow, keyboardHeight ->
            if (!isShow) {
                mSearchInput.clearFocus()
            }
        }
    }

    override fun onDetachedFromWindow() {
        //在销毁之前注销键盘监听
        keyboardListener?.reset()
        super.onDetachedFromWindow()
    }
}