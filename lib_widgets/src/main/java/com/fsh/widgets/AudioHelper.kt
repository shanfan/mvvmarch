package com.fsh.widgets

import android.content.Context
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.util.Log
import java.io.File

class AudioHelper : VoiceInputView.OnInputListener , Handler.Callback{
    private var audioManager:AudioManager? = null
    private var mContext:Context? = null
    private var mAfChangeListener:AudioManager.OnAudioFocusChangeListener? = null
    private var mMediaRecord:MediaRecorder? = null
    private var mOutputFile:Uri? = null
    private var mHandler:Handler = Handler(Looper.getMainLooper(),this)
    private var inputView: VoiceInputView? = null
    fun bindView(voiceInputView: VoiceInputView){
        voiceInputView.onInputListener = this
        inputView = voiceInputView
        mContext = voiceInputView.context
        audioManager = mContext?.getSystemService(Context.AUDIO_SERVICE) as? AudioManager
    }

    private fun startRecord(){
        mAfChangeListener?.let {
            audioManager?.abandonAudioFocus(it)
        }
        mAfChangeListener = AudioManager.OnAudioFocusChangeListener {
            if(it == AudioManager.AUDIOFOCUS_LOSS){
                audioManager?.abandonAudioFocus(mAfChangeListener)
                mAfChangeListener = null
                mHandler.sendEmptyMessage(MSG_RECORD_CANCEL)
            }
        }
        mHandler.sendEmptyMessage(MSG_RECORD_START)
        mHandler.sendMessageDelayed(Message.obtain(mHandler, MSG_RECORD_TIME_OUT), RECORD_TIME_OUT)
    }

    private fun startRec(){
        muteAudioFocus(true)
        audioManager?.mode = AudioManager.MODE_NORMAL
        mMediaRecord = MediaRecorder()
        //设置采样频率
        mMediaRecord?.setAudioSamplingRate(44100)
        //设置码率
        mMediaRecord?.setAudioEncodingBitRate(16000)
        //设置通道
        mMediaRecord?.setAudioChannels(1)
        //设置音频来源：麦克风
        mMediaRecord?.setAudioSource(MediaRecorder.AudioSource.MIC)
        //设置输出文件格式
        mMediaRecord?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        //设置编码格式
        if(Build.VERSION.SDK_INT >= 28){
            mMediaRecord?.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC)
        }else{
            mMediaRecord?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        }
        //文件保存路径
        val saveDir = mContext?.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
        val outputFile = Uri.fromFile(File(saveDir, "VOICE_".plus(System.currentTimeMillis().toString()).plus(".aac")))
        mOutputFile = outputFile
        mMediaRecord?.setOutputFile(outputFile.path)
        mMediaRecord?.prepare()
        mMediaRecord?.start()

    }

    override fun onPressed() {
        Log.i("AudioHelper","onPressed")
        startRecord()
    }

    override fun onCancel() {
        Log.i("AudioHelper","onCancel")
        mHandler.sendEmptyMessage(MSG_RECORD_CANCEL)
    }

    override fun onComplete() {
        Log.i("AudioHelper","onComplete")
        mHandler.removeMessages(MSG_RECORD_TIME_OUT)
        mHandler.sendEmptyMessage(MSG_RECORD_COMPLETE)
    }

    private companion object{
        //最多记录1分钟的录音
        const val RECORD_TIME_OUT = 3 * 1000L
        //等300毫秒再去删除文件
        const val TIME_DELETE_FILE = 300L
        //开始录音
        const val MSG_RECORD_START = 1
        //取消录音
        const val MSG_RECORD_CANCEL = 2
        //录音超市
        const val MSG_RECORD_TIME_OUT = 3
        //录音完成
        const val MSG_RECORD_COMPLETE = 4
        //删除文件
        const val MSG_DELETE_FILE = 5
    }

    override fun handleMessage(msg: Message): Boolean {
        Log.d("AudioHelper", "handleMessage: ${msg.what}")
        when(msg.what){
            MSG_RECORD_START -> onRecordStart()
            MSG_RECORD_CANCEL -> onRecordCancel()
            MSG_RECORD_TIME_OUT -> onRecordTimeOut()
            MSG_RECORD_COMPLETE -> onRecordComplete()
            MSG_DELETE_FILE -> onDeleteFile()
        }
        return true
    }

    private fun onRecordStart(){
        try{
            startRec()
        }catch (e: Exception){
            Log.e("AudioHelper", "startRec fail", e)
        }
    }

    private fun onRecordCancel(){
        stopRec()
        Log.i("AudioHelper","onRecordCancel")
        //这里不能立马去删除文件,需要等录音进程结束对文件的引用之后去删除才能删除成功
        mHandler.sendEmptyMessageDelayed(MSG_DELETE_FILE, TIME_DELETE_FILE)
    }
    private fun onRecordTimeOut(){
        Log.i("AudioHelper","onRecordTimeOut")
        onRecordCancel()
//        inputView?.cancel()
    }

    private fun onRecordComplete(){
        Log.i("AudioHelper","onRecordComplete")
        stopRec()
        mOutputFile?.run {
            Log.i("AudioHelper","outputFile path $path")
        }
    }

    private fun onDeleteFile(){
        Log.i("AudioHelper","onDeleteFile ${mOutputFile?.path}")
        mOutputFile?.run {
            val file = File(path?:"")
            if(file.exists()){
                val result = file.delete()
                Log.i("AudioHelper","delete file $result")
            }else{
                Log.i("AudioHelper","delete fail,file not exit")
            }

        }
    }

    private fun stopRec(){
        try{
            muteAudioFocus(false)
            mMediaRecord?.let {
                it.stop()
                it.release()
            }
            mMediaRecord = null
        }catch (e: Exception){
            Log.e("AudioHelper", "stopRec fail", e)
        }
    }

    private fun muteAudioFocus(bMute: Boolean){
        mAfChangeListener?.let {
            if(bMute){
                audioManager?.requestAudioFocus(mAfChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
            }else{
                audioManager?.abandonAudioFocus(mAfChangeListener)
                mAfChangeListener = null
            }
        }
    }
}